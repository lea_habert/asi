package asi.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import asi.dao.Dao;
import asi.model.Article;
import asi.model.Categorie;

@Path("/articles")
public class ArticlesResources {
	// Allows to insert contextual objects into the class,
    // e.g. ServletContext, Request, Response, UriInfo
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Article> getAllArticles() {
		List<Article> articles = new ArrayList<Article>();
		articles.addAll(Dao.instance.getModelArticle().values());
		return articles;
	}
	
	// Defines that the next path parameter after articles is
    // treated as a parameter and passed to the ArticlesResource
    // Allows to type http://localhost:8080/asi/Boutique/article/1
    // 1 will be treaded as parameter article and passed to ArticleResource
    @Path("{article}")
    public ArticleResource getArticleById(@PathParam("article") String id) {
        return new ArticleResource(uriInfo, request, id);
    }
    
    @Path("{article}/delete")
    public ArticleResource removeArticleById(@PathParam("article") String id) {
    	return new ArticleResource(uriInfo, request, id).deleteArticle();
    }
    
    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void newArticle(@FormParam("id") int id,
            @FormParam("libelle") String libelle,
            @FormParam("prix") double prix,
            @FormParam("photo") String photo, 	
            @FormParam("categorie") String categorie,
            @Context HttpServletResponse servletResponse) throws IOException {
    	Categorie myCategorie = null;
    	for(String mapKey:Dao.instance.getModelCategorie().keySet()) {
    		//System.out.println("MY MAPKEY : "+mapKey.getClass().getName()+" CATEGORIE : "+categorie.getClass().getName());
    		for(Article a: Dao.instance.getModelCategorie().get(mapKey).getArticleList()) {
    			//System.out.println("mon id de a : "+a.getIdArticle());
    			if(a.getIdArticle()==id) {
    				//System.out.println("je suis dans if");
    				Dao.instance.getModelCategorie().get(mapKey).getArticleList().remove(a);
    				break;
    			}
    		}
    		
    		if(mapKey.equals(categorie)) 
    			myCategorie = Dao.instance.getModelCategorie().get(mapKey);
    	}
    	/*
    	for(Article a: myCategorie.getArticleList()) {
    		if(a.getIdArticle()==id) {
    			Dao.instance.getModelCategorie().get(categorie).getArticleList().remove(a);
    			break;
    		}
    	}
    	*/
        Article article = new Article(id, libelle, prix, photo, myCategorie);
        Dao.instance.getModelArticle().put(String.valueOf(id), article);

        servletResponse.sendRedirect("http://localhost:8080/asi/Boutique/articles");
    }
    
    @PUT
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void updateArticle(@FormParam("id") int id,
            @FormParam("libelle") String libelle,
            @FormParam("prix") double prix,
            @FormParam("photo") String photo, 	
            @FormParam("categorie") String categorie,
            @Context HttpServletResponse servletResponse) throws IOException {
    	Categorie myCategorie = null;
    	for(String mapKey:Dao.instance.getModelCategorie().keySet()) {
    		//System.out.println("MY MAPKEY : "+mapKey.getClass().getName()+" CATEGORIE : "+categorie.getClass().getName());
    		if(mapKey.equals(categorie)) 
    			myCategorie = Dao.instance.getModelCategorie().get(mapKey);
    	}
        Article article = new Article(id, libelle, prix, photo, myCategorie);
        Dao.instance.getModelArticle().put(String.valueOf(id), article);
        
        /*
        for(Article a:Dao.instance.getModelCategorie().get(categorie).getArticleList()) {
        	System.out.println("libelle de a : "+a.getLibelle());
        	if(a.getIdArticle()==id) {
        		System.out.println("je suis dans le if");
        		a.setLibelle(libelle);
        	}
        }
        */

        servletResponse.sendRedirect("http://localhost:8080/asi/Boutique/articles");
    }
    
    
    /*
    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_JSON)
    public void newArticle(Article article) {
    	Dao.instance.getModelArticle().put(String.valueOf(article.getIdArticle()), article);
    }
    */
    
    /*
    @PUT
    //@Path("{article}/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateArticleById(@PathParam("article") String id,
    		@FormParam("libelle") String libelle,
            @FormParam("prix") double prix,
            @FormParam("photo") String photo,
            @FormParam("categorie") String categorie) {
    	Article article = Dao.instance.getModelArticle().get(id);
    	
    	 if(article==null)
             throw new RuntimeException("Put: Article with " + id +  " not found");
    	 
    	 article.setLibelle(libelle);
    	 article.setPrix(prix);
    	 article.setPhoto(photo);
    	 
    	 Categorie myCategorie = null;
     	for(String mapKey:Dao.instance.getModelCategorie().keySet()) {
     		//System.out.println("MY MAPKEY : "+mapKey.getClass().getName()+" CATEGORIE : "+categorie.getClass().getName());
     		if(mapKey.equals(categorie)) 
     			myCategorie = Dao.instance.getModelCategorie().get(mapKey);
     	}
    	 article.addCategorie(myCategorie);
    	 
    	 return Response.noContent().build();
    }
    */
}
