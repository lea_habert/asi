package asi.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import asi.dao.Dao;
import asi.model.Administrateur;

@Path("/administrateur")
public class AdministrateurResource {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Administrateur> getAdministrateur() {
		List<Administrateur> administrateurs = new ArrayList<Administrateur>();
		administrateurs.addAll(Dao.instance.getModelAdministrateur().values());
		return administrateurs;
	}
}
