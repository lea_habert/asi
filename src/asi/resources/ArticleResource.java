package asi.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import asi.dao.Dao;
import asi.model.Article;

public class ArticleResource {
	@Context
    UriInfo uriInfo;
    @Context
    Request request;
    String id;
    public ArticleResource(UriInfo uriInfo, Request request, String id) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.id = id;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Article getArticleById() {
        Article article = Dao.instance.getModelArticle().get(id);
        if(article==null)
            throw new RuntimeException("Get: Article with " + id +  " not found");
        return article;
    }
    
    @DELETE
    public ArticleResource deleteArticle() {
    	Article article = Dao.instance.getModelArticle().get(id);
    	for(String mapKey:Dao.instance.getModelCategorie().keySet()) {
    		if(Dao.instance.getModelCategorie().get(mapKey).getArticleList().contains(article)) {
    			Dao.instance.getModelCategorie().get(mapKey).getArticleList().remove(article);
    		}
    	}
        Dao.instance.getModelArticle().remove(id);
        /*
        if(article==null)
            throw new RuntimeException("Delete: Article with " + id +  " not found");
            */
        
		return null;
    }
    
    /*
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putArticle(@FormParam("libelle") String libelle,
            @FormParam("prix") double prix,
            @FormParam("photo") String photo,
            @FormParam("categorie") String categorie) {
        return Response.noContent().build();
    }
    */
    
    /*
    private Response putAndGetResponse(Article article) {
        Response res;
        if(Dao.instance.getModelArticle().containsKey(String.valueOf(article.getIdArticle()))) {
            res = Response.noContent().build();
        } 
        else {
            res = Response.created(uriInfo.getAbsolutePath()).build();
        }
        Dao.instance.getModelArticle().put(String.valueOf(article.getIdArticle()), article);
        return res;
    }
    */
}
