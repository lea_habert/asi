package asi.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import asi.dao.Dao;
import asi.model.Article;
import asi.model.Categorie;

@Path("/categories")
public class CategoriesResources {
	// Allows to insert contextual objects into the class,
    // e.g. ServletContext, Request, Response, UriInfo
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    
    @GET
	@Produces(MediaType.APPLICATION_JSON)
    public List<Categorie> getAll(){
		List<Categorie> listCategories = new ArrayList<Categorie>();
        listCategories.addAll(Dao.instance.getModelCategorie().values());
        return listCategories;
	}
    
    @Path("/parents")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Categorie> getAllCategories(){
		List<Categorie> listCategories = new ArrayList<Categorie>();
        for(String mapKey:Dao.instance.getModelCategorie().keySet()) {
            if(Dao.instance.getModelCategorie().get(mapKey).getCategorieList().size() > 0) {
                listCategories.add(Dao.instance.getModelCategorie().get(mapKey));
            }
        }
        //listCategories.addAll(Dao.instance.getModelCategorie().values());
        return listCategories;
	}
	
	// Defines that the next path parameter after articles is
    // treated as a parameter and passed to the ArticlesResource
    // Allows to type http://localhost:8080/asi/Boutique/article/1
    // 1 will be treaded as parameter article and passed to ArticleResource
    @Path("{categorie}")
    public CategorieResource getCategorieById(@PathParam("categorie") String id) {
        return new CategorieResource(uriInfo, request, id);
    }
    
    /*
    @Path("{categorie}/details")
    public Categorie getCategorieDetails(@PathParam("categorie") String id) {
        return new CategorieResource(uriInfo, request, id).getCategorieById();
    }
    */
    
    @Path("{categorie}/delete")
    public CategorieResource removeCategorieById(@PathParam("categorie") String id) {
    	return new CategorieResource(uriInfo, request, id).deleteCategorie();
    }
    
    @Path("{categorie}/articles")
    public List<Article> getArticlesByCategorie(@PathParam("categorie") String id) {
    	return new CategorieResource(uriInfo, request, id).getArticlesByCategorie();
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void newArticle(@FormParam("id") String id,
            @FormParam("libelle") String libelle,
            @FormParam("categorieParent") String categorieParent,
            @Context HttpServletResponse servletResponse) throws IOException {
    	
        Categorie categorie = new Categorie(id, libelle);
        if(!categorieParent.equals("undefined")) {
        	Categorie myCategorieParent = null;
        	for(String mapKey:Dao.instance.getModelCategorie().keySet()) {
        		if(mapKey.equals(categorieParent)) 
        			myCategorieParent = Dao.instance.getModelCategorie().get(mapKey);
        	}
        	myCategorieParent.addCategorieToList(categorie);
        }
        
        Dao.instance.getModelCategorie().put(String.valueOf(id), categorie);

        servletResponse.sendRedirect("http://localhost:8080/asi/Boutique/categories");
    }
}
