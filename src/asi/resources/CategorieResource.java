package asi.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import asi.dao.Dao;
import asi.model.Article;
import asi.model.Categorie;

public class CategorieResource {
	@Context
    UriInfo uriInfo;
    @Context
    Request request;
    String id;
    public CategorieResource(UriInfo uriInfo, Request request, String id) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.id = id;
    }
    
    /*
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Categorie getCategorieById() {
        Categorie categorie = Dao.instance.getModelCategorie().get(id);
        if(categorie==null)
            throw new RuntimeException("Get: Categorie with " + id +  " not found");
        return categorie;
    }
	*/

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Article> getArticlesByCategorie() {
		List<Article> listArticles = new ArrayList<Article>();
        listArticles.addAll(Dao.instance.getModelCategorie().get(id).getArticleList());
        //if(categorie==null)
          //  throw new RuntimeException("Get: Categorie with " + id +  " not found");
        return listArticles;
    }
    
    @DELETE
    public CategorieResource deleteCategorie() {
        Categorie categorie = Dao.instance.getModelCategorie().remove(id);
        if(categorie==null)
            throw new RuntimeException("Delete: Categorie with " + id +  " not found");
        
		return null;
    }
}
