package asi.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import asi.dao.Dao;
import asi.model.Boutique;

@Path("/boutique")
public class BoutiqueResource {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Boutique> getBoutique() {
		List<Boutique> boutique = new ArrayList<Boutique>();
		boutique.addAll(Dao.instance.getModelBoutique().values());
		return boutique;
	}
}
