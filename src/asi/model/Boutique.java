package asi.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Boutique {
	private int idBoutique;
	private String description;
	private String adresse;
	private String mail;
	private String telephone;
	
	//Constructeurs
	public Boutique() {
		
	}
		
	public Boutique(int idBoutique, String description, String adresse, String mail, String telephone) {
		this.idBoutique = idBoutique;
		this.description = description;
		this.adresse = adresse;
		this.mail = mail;
		this.telephone = telephone;
	}
	
	//GETTERS
	public int getIdBoutique() {
		return idBoutique;
	}
	public String getDescription() {
		return description;
	}
	public String getAdresse() {
		return adresse;
	}
	public String getMail() {
		return mail;
	}
	public String getTelephone() {
		return telephone;
	}
}
