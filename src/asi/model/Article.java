package asi.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;

@XmlRootElement
public class Article {
	private int idArticle;
	private String libelleArticle;
	private double prix;
	private String photo;
	@JsonBackReference
	public List<Categorie> categorieList;
	
	//Constructeurs
	public Article() {
	
	}
	
	public Article(int idArticle, String libelleArticle, double prix, String photo, 
			Categorie categorie) {
		this.categorieList = new ArrayList<>();
		this.idArticle = idArticle;
		this.libelleArticle = libelleArticle;
		this.prix = prix;
		this.photo = photo;
		this.addCategorie(categorie);
	}
	
	//Getters
	/*
	public List<Categorie> getCategorie() {
		return categorieList;
	}
	*/
	
	public int getIdArticle() {
		return idArticle;
	}
	public String getLibelle() {
		return libelleArticle;
	}
	public double getPrix() {
		return prix;
	}
	public String getPhoto() {
		return photo;
	}
	
	//Setters
	public void setLibelle(String libelleArticle) {
		this.libelleArticle = libelleArticle;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public void replaceCategorie(Categorie oldCat, Categorie newCat) {
		this.removeCategorie(oldCat);
		this.addCategorie(newCat);
	}
	
	public void addCategorie(Categorie categorie) {
		this.categorieList.add(categorie);
		categorie.getArticleList().add(this);
	}
	
	public void removeCategorie(Categorie categorie) {
		this.categorieList.remove(categorie);
		categorie.getArticleList().remove(this);
	}	
}
