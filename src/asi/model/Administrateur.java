package asi.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Administrateur {
	private String nom;
	private String prenom;
	
	//Constructeurs
	public Administrateur() {
		
	}
	
	public Administrateur(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}

	//Getters
	public String getNom() {
		return this.nom;
	}
	
	public String getPrenom() {
		return this.prenom;
	}
	
	/*
	public String getEmail() {
		return this.email;
	}
	
	public String getPwd() {
		return this.pwd;
	}
	*/
}
