package asi.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

//import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@XmlRootElement
public class Categorie {
	private String idCategorie;
	private String libelleCategorie;
	@JsonManagedReference
	private List<Article> articleList;
	//@JsonIgnore
	private List<Categorie> categorieList;
	
	//Constructeurs
	public Categorie() {
		
	}
	
	public Categorie(String idCategorie, String libelleCategorie)
	{
		articleList = new ArrayList<>();
		categorieList = new ArrayList<>();
		this.idCategorie = idCategorie;
		this.libelleCategorie = libelleCategorie;
	}
	
	//Getters
	public List<Article> getArticleList(){
		return articleList;
	}
	
	public List<Categorie> getCategorieList() {
		return this.categorieList;
	}

	public String getIdCategorie() {
		return this.idCategorie;
	}
	
	public String getLibelleCategorie() {
		return this.libelleCategorie;
	}
	
	//Setters
	public void setLibelleCategorie(String libelleCategorie) {
		this.libelleCategorie = libelleCategorie;
	}
	
	public void addCategorieToList(Categorie categorie) {
		this.categorieList.add(categorie);
	}
	
	public void removeCategorieFromList(Categorie categorie) {
		this.categorieList.remove(categorie);
	}
	
	public void addArticle(Article article) {
		this.articleList.add(article);
		article.categorieList.add(this);
	}
	
	public void removeArticle(Article article) {
		this.articleList.remove(article);
		article.categorieList.remove(this);
	}
}
