package asi.dao;

import java.util.HashMap;
import java.util.Map;

import asi.model.Administrateur;
import asi.model.Article;
import asi.model.Boutique;
import asi.model.Categorie;

public enum Dao {
	instance;
	
	private Map<String, Article> contentProviderArticle;
	private Map<String, Categorie> contentProviderCategorie;
	private Map<String, Administrateur> contentProviderAdministrateur;
	private Map<String, Boutique> contentProviderBoutique;
	
	private Dao() {
		contentProviderArticle = new HashMap<>();
		contentProviderCategorie = new HashMap<>();
		contentProviderAdministrateur = new HashMap<>();
		contentProviderBoutique = new HashMap<>();
		
		Boutique boutique = new Boutique(1, "Boutique Principale", "Paris", "boutique@contact.fr",
				"0100000000");
		contentProviderBoutique.put("1", boutique);
		
		Administrateur admin = new Administrateur("Dubois", "Tom");
		contentProviderAdministrateur.put("1", admin);
		
		Categorie c1 = new Categorie("1", "Ordinateurs");
		contentProviderCategorie.put("1", c1);
		
		Categorie c1_1 = new Categorie("2", "PC Portable");
		c1.addCategorieToList(c1_1);
		contentProviderCategorie.put("2", c1_1);
		
		Categorie c1_2 = new Categorie("3", "PC Bureau");
		c1.addCategorieToList(c1_2);
		contentProviderCategorie.put("3", c1_2);
		
		Categorie c1_3 = new Categorie("4", "PC Accesoires");
		c1.addCategorieToList(c1_3);
		contentProviderCategorie.put("4", c1_3);
		
		Categorie c2 = new Categorie("5", "Téléphonies");
		contentProviderCategorie.put("5", c2);
		
		Categorie c2_1 = new Categorie("6", "Smartphone");
		c2.addCategorieToList(c2_1);
		contentProviderCategorie.put("6", c2_1);
		
		Categorie c2_2 = new Categorie("7", "Téléphone Fixe");
		c2.addCategorieToList(c2_2);
		contentProviderCategorie.put("7", c2_2);
		
		Categorie c2_3 = new Categorie("8", "Tel Accessoires");
		c2.addCategorieToList(c2_3);
		contentProviderCategorie.put("8", c2_3);
		
		Categorie c3 = new Categorie("9", "Stockage");
		contentProviderCategorie.put("9", c3);
		
		Categorie c3_1 = new Categorie("10", "Disque Dur");
		c3.addCategorieToList(c3_1);
		contentProviderCategorie.put("10", c3_1);
		
		Categorie c3_2 = new Categorie("11", "USB");
		c3.addCategorieToList(c3_2);
		contentProviderCategorie.put("11", c3_2);
		
		Categorie c3_3 = new Categorie("12", "Stockage Accessoires");
		c3.addCategorieToList(c3_3);
		contentProviderCategorie.put("12", c3_3);
		
		Article a1 = new Article(1, "Ordinateur Portable ASUS", 500.0, "assets/images/ordinateurs/PcPASUS.jpg", c1_1);
		contentProviderArticle.put("1", a1);		
		Article a2 = new Article(2, "Ordinateur Portable Apple", 1000.0, "assets/images/ordinateurs/PcPApple.jpg", c1_1);
		contentProviderArticle.put("2", a2);
		Article a3 = new Article(3, "Ordinateur Portable Lenovo", 100.0, "assets/images/ordinateurs/PcPLenovo.jpg", c1_1);
		contentProviderArticle.put("3", a3);
		Article a4 = new Article(4,"Ordinateur Portable", 230.0,"assets/images/ordinateurs/PcPortable.png", c1_1);
		contentProviderArticle.put("4", a4);		
		Article a5 = new Article(5,"Ordinateur Fixe", 679.0, "assets/images/ordinateurs/PcBureau.jpg", c1_2);
		contentProviderArticle.put("5", a5);		
		Article a6 = new Article(6,"Support Ordinateur Portable", 65.0, "assets/images/ordinateurs/accessoires.jpg", c1_3);
		contentProviderArticle.put("6", a6);
		Article a7 = new Article(7, "Souris Apple", 10.0, "assets/images/ordinateurs/access2.jpg", c1_3);
		contentProviderArticle.put("7", a7);
		
		
		Article a8 = new Article(8,"Telephone fixe",53.0,"assets/images/phones/fixe.jpg", c2_2);
		contentProviderArticle.put("8", a8);		
		Article a9 = new Article(9, "Telephone fixe ancien", 70.0, "assets/images/phones/fixe2.jpg", c2_2);
		contentProviderArticle.put("9", a9);
		Article a10 = new Article(10, "Smartphone", 670.0, "assets/images/phones/portable.jpg", c2_1);
		contentProviderArticle.put("10", a10);
		Article a11 = new Article(11, "Portable Samsung", 347.0, "assets/images/phones/portableSamsung.jpg", c2_1);
		contentProviderArticle.put("11", a11);
		Article a12 = new Article(12, "Batterie externe", 41.0, "assets/images/phones/batterie.jpg", c2_3);
		contentProviderArticle.put("12", a12);
		Article a13 = new Article(13, "Casque", 27.0, "assets/images/phones/casque.jpg", c2_3);
		contentProviderArticle.put("13", a13);
		
		Article a14 = new Article(14, "Cle USB forme de cle", 61.0, "assets/images/stockage/cle2.jpg", c3_2);
		contentProviderArticle.put("14", a14);
		Article a15 = new Article(15, "Cle USB compacte", 39.0, "assets/images/stockage/cle.jpg", c3_2);
		contentProviderArticle.put("15", a15);
		Article a16 = new Article(16, "Disque dur externe 1To", 89.0, "assets/images/stockage/disque.jpg", c3_1);
		contentProviderArticle.put("16", a16);
		Article a17 = new Article(17, "Disque dur externe", 75.0, "assets/images/stockage/disque2.jpg", c3_1);
		contentProviderArticle.put("17", a17);
		Article a18 = new Article(18, "Carte SD 16GB", 56.0, "assets/images/stockage/carteSD.jpg", c3_3);
		contentProviderArticle.put("18", a18);
	}
	
	public Map<String, Article> getModelArticle() {
		return contentProviderArticle;
	}
	
	public Map<String, Categorie> getModelCategorie() {
		return contentProviderCategorie;
	}
	
	public Map<String, Administrateur> getModelAdministrateur() {
		return contentProviderAdministrateur;
	}
	
	public Map<String, Boutique> getModelBoutique() {
		return contentProviderBoutique;
	}
}
